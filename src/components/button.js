import React from 'react'
import styles from './button.scss'

export default () => {
  return (
    <button className={styles.button}>Click Me</button>
  )
}
