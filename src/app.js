import React from 'react'
import { Heading, Deck, Slide } from 'spectacle'
import Hello from './hello.mdx'

export default class Presentation extends React.Component {
  render () {
    return (
      <Deck>
        <Slide>
          <Heading size='1'>Hello</Heading>
        </Slide>
        <Slide>
          <Heading size='2'>World</Heading>
        </Slide>
        <Slide>
          <Heading size='3'>Jupiter</Heading>
        </Slide>
        <Hello />
      </Deck>
    )
  }
}
